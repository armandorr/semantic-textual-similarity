import nltk
import spacy
from nltk.stem import WordNetLemmatizer
from Preprocessing import preprocessing

stopWords = set(nltk.corpus.stopwords.words('english'))
wnl = WordNetLemmatizer()
nlp = spacy.load("en_core_web_sm")


# Tokens
# Sentence tokenizers using or not the preprocessing
def tokenizeSentences(sentence0, sentence1):
    tokenizedSentence0 = set(nltk.word_tokenize(sentence0))
    tokenizedSentence1 = set(nltk.word_tokenize(sentence1))
    return tokenizedSentence0, tokenizedSentence1

def tokenizeSentencesBestTokens(sentence0, sentence1):
    tokenizedSentence0, tokenizedSentence1 = preprocessing(
        sentence0, sentence1, pos_tagger=False)
    tokenizedSentence0 = set(tokenizedSentence0)
    tokenizedSentence1 = set(tokenizedSentence1)
    return tokenizedSentence0, tokenizedSentence1


# Lemmas
# Sentence lemmatization using NLTK and Spacy

# Auxiliar function to NLTK lemmatizer
def _lemmatizeWordsNltk(wordsIn):
    pairs = preprocessing(wordsIn)

    lemmatizedWordsList = []
    for word, tag in pairs:
        lemmatizedWord = word.lower()
        tagLowered = tag.lower()

        pos = tagLowered[0]  # Noun ==> N,  Verbs ==> V
        if pos == 'j':
            pos = 'a'  # Adjective = J ==> A
        if tagLowered[:2] == 'rb':
            pos = 'r'  # Adverb = RB ==> R
        if pos in ['n', 'v', 'a', 'r']:
            lemmatizedWord = wnl.lemmatize(lemmatizedWord, pos=pos)
        lemmatizedWordsList.append(lemmatizedWord)

    return set(lemmatizedWordsList)

# Main NLTK lemmatizer
def lemmasNltk(sentence0, sentence1):
    lemmatizedSentence0 = _lemmatizeWordsNltk(sentence0)
    lemmatizedSentence1 = _lemmatizeWordsNltk(sentence1)
    return lemmatizedSentence0, lemmatizedSentence1

# Auxiliar function to spacy lemmatizer
def _lemmatizeWordsSpaCy(sentence):
    doc = nlp(str(sentence))
    lemmatizedWordsList = [(token.lemma_.lower()) for token in doc
                           if not (token.is_stop or token.pos_ == 'PUNCT')]
    return set(lemmatizedWordsList)

# Main Spacy lemmatizer
def lemmasSpacy(sentence0, sentence1):
    lemmatizedSentence0 = _lemmatizeWordsSpaCy(sentence0)
    lemmatizedSentence1 = _lemmatizeWordsSpaCy(sentence1)
    return (lemmatizedSentence0, lemmatizedSentence1)


# Character N-Grams
# Get the n-grams (e.g. unigrams, bigrams, trigrams, etc.) overlap
# of the words in a sentence according to the specified n
def nGramsOverlap(sentence0, sentence1, n):
    tokens0, tokens1 = preprocessing(sentence0, sentence1, pos_tagger=False)

    tokens0 = " ".join(tokens0)
    tokens1 = " ".join(tokens1)

    ngrams0 = set(nltk.ngrams(tokens0, n))
    ngrams1 = set(nltk.ngrams(tokens1, n))
    intersection = ngrams0.intersection(ngrams1)
    return 2 * len(intersection) / (len(ngrams0) + len(ngrams1))
