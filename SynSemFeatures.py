import nltk
import spacy
import numpy as np
from numpy.linalg import norm
from itertools import product
from nltk.corpus import wordnet_ic
from nltk.corpus import wordnet as wn
from Preprocessing import preprocessing
from nltk.corpus.reader.wordnet import information_content
nltk.download('wordnet_ic')
nltk.download('stopwords')
brown_ic = wordnet_ic.ic('ic-brown.dat')
stopWords = set(nltk.corpus.stopwords.words('english'))
nlp = spacy.load("en_core_web_sm")


# Lexical Semantics

# Auxiliar function for lexical semantics
def _getMostFrequentSynsets(sentence):
    mostFrequentSynsets = []
    for word, tag in sentence:
        synset = wn.synsets(word, tag)
        if len(synset) > 0:
            mostFrequentSynsets.append((synset[0].pos(), synset[0]))
    return mostFrequentSynsets

# Find the average similarity measure between two sentences using the indicated method, which could be:
# Path, Leackock-Chodorow, Wu-Palmer, or Lin similarities,


def lexicalSemantics(sentence0, sentence1, sim="pa"):
    validTags = ['n', 'v', 'a', 'r']
    pairs0, pairs1 = preprocessing(sentence0, sentence1)
    pairs0 = [(word, tag) for word, tag in pairs0 if tag in validTags]
    pairs1 = [(word, tag) for word, tag in pairs1 if tag in validTags]

    synsets0 = _getMostFrequentSynsets(pairs0)
    synsets1 = _getMostFrequentSynsets(pairs1)

    similarities = []

    # Traverse all the combinations of words for both sentences
    # and compute the average similarity for all of them
    combinations = product(synsets0, synsets1)
    for (tag0, synset0), (tag1, synset1) in combinations:
        if synset0 == synset1:
            similarities.append(1)
            continue

        if sim == "pa":  # Path similarity
            similarities.append(synset0.path_similarity(synset1))

        if sim == "lc":  # Leacock-Chodorow Similarity
            if tag0 == tag1:
                normalizeValue = synset0.lch_similarity(synset0)
                similarities.append(
                    synset0.lch_similarity(synset1)/normalizeValue)

        if sim == "wu":  # Wu-Palmer Similarity
            similarities.append(synset0.wup_similarity(synset1))

        if sim == "li":  # Lin Similarity
            if tag0 == tag1 and tag0 in ['n', 'v']:
                similarities.append(synset0.lin_similarity(synset1, brown_ic))

    return np.mean(similarities) if len(similarities) != 0 else 0


# Lesk
# Auxiliar function for the lesk function
def _leskSentence(sentence):
    pairs = preprocessing(sentence)

    output = []
    for (word, tag) in pairs:
        if tag in ['n', 'v', 'a', 'r']:
            value = nltk.wsd.lesk(sentence, word, tag)
            if value:
                output.append(value.name())
            else:
                output.append(word.lower())
        elif word not in stopWords:
            output.append(word.lower())

    return set(output)

# Obtain the lesk, word definition using word sense disambiguation of the applicable words in the sentences


def lesk(sentence0, sentence1):
    leskSentence0 = _leskSentence(sentence0)
    leskSentence1 = _leskSentence(sentence1)
    return (leskSentence0, leskSentence1)


# Name entities
# Auxiliar function of the following name entities function
def _nameEntitiesSpacySentence(sentence):
    doc = nlp(str(sentence))
    with doc.retokenize() as retokenizer:
        tokens = [token for token in doc]
        for ent in doc.ents:
            retokenizer.merge(doc[ent.start:ent.end],
                              attrs={"LEMMA": " ".join([tokens[i].text for i in range(ent.start, ent.end)])})
    texts = [((token.text).lower())
             for token in doc if not (token.is_stop or token.pos_ == 'PUNCT')]
    return set(texts)

# Compute the sets of name entities for two sentences


def nameEntitiesSpacy(sentence0, sentence1):
    nameEntitiesSentence0 = _nameEntitiesSpacySentence(sentence0)
    nameEntitiesSentence1 = _nameEntitiesSpacySentence(sentence1)
    return (nameEntitiesSentence0, nameEntitiesSentence1)


# WordNet-AugmentedWord Overlap
# Auxiliar function that computes the score for a word in a sentence.
# To score it make use of the maximum path similarity among all the words.
def _scoreWordSentenceOverlap(wEntry, wtag, S, pairs):
    if wEntry in S:
        return 1

    maximum = None
    wSynset = wn.synsets(wEntry, wtag)
    if len(wSynset) > 0:
        wSynset = wSynset[0]
        for wAux, tag in pairs:
            wAuxSynset = wn.synsets(wAux, tag)
            if len(wAuxSynset) > 0:
                value = wSynset.path_similarity(wAuxSynset[0])
                if maximum is not None:
                    maximum = max(value, maximum)
                else:
                    maximum = value

    return maximum if maximum is not None else 0

# Auxiliar function that performs the needed loop for all the words computing the sum of scores


def _computeWNAOverlap_aux(pairs0, words1, pairs1):
    coverage = 0
    for word, tag in pairs0:
        coverage += _scoreWordSentenceOverlap(word, tag, words1, pairs1)
    # Inverted for harmonic mean
    return len(words1)/coverage if coverage != 0 else 0

# Compute WordNet-AugmentedWord Overlap for two specified sentences


def computeWNAOverlap(sentence0, sentence1):
    validTags = ['n', 'v', 'a', 'r']
    pairs0, pairs1 = preprocessing(sentence0, sentence1)
    pairs0 = [(word, tag) for word, tag in pairs0 if tag in validTags]
    pairs1 = [(word, tag) for word, tag in pairs1 if tag in validTags]

    words0 = [word for (word, tag) in pairs0]
    words1 = [word for (word, tag) in pairs1]

    one = _computeWNAOverlap_aux(pairs0, words1, pairs1)
    two = _computeWNAOverlap_aux(pairs1, words0, pairs0)
    result = one + two
    return 2/result if result != 0 else 0


# Weighted Word Overlap
# Auxiliar function that computes the information content for the words
# in the intersection and the words of pairs1.
def _computeWWOverlap_aux(pairs0, pairs1):
    pairs0_set, pairs1_set = set(pairs0), set(pairs1)
    union = pairs0_set.union(pairs1_set)
    intersection = pairs0_set.intersection(pairs1_set)

    numerator, denominator = 0, 0
    for pair in union:
        if pair not in pairs1:
            continue
        wsynset = wn.synsets(pair[0], pair[1])
        ic = 0
        if len(wsynset) > 0:
            ic = information_content(wsynset[0], brown_ic)
        denominator += ic
        if pair in intersection:
            numerator += ic

    # inverted for the harmonic mean
    return denominator/numerator if numerator != 0 else 0

# Main function that computes the Weighted Word Overlap for two sentences


def computeWWOverlap(sentence0, sentence1):
    validTags = ['n', 'v']
    pairs0, pairs1 = preprocessing(sentence0, sentence1)
    pairs0 = [(word, tag) for word, tag in pairs0 if tag in validTags]
    pairs1 = [(word, tag) for word, tag in pairs1 if tag in validTags]

    one = _computeWWOverlap_aux(pairs0, pairs1)
    two = _computeWWOverlap_aux(pairs1, pairs0)
    result = one + two
    return 2/result if result != 0 else 0


# Vector Space Sentence Similarity
# Auxiliar function that for a sentence compute its vector space array
def _createVectorSpaceArray(sentence, allWords, ic):
    vector = np.zeros(len(allWords))
    for (word, tag) in set(sentence):
        total = len([word_aux for (word_aux, tag)
                    in sentence if word_aux == word])
        if ic and tag in ["n", "v"]:
            # improve by getting all the sims
            wsynset = wn.synsets(word.lower(), tag)
            if len(wsynset) > 0:
                ic_val = information_content(wsynset[0], brown_ic)
                ic_val = min(ic_val, 10)
                total *= ic_val

        vector[np.where(np.all(np.array(allWords) ==
                        (word, tag), axis=1))] = total

    return vector

# Main function that computes the vector space similarity for two sentences using the cosine


def computeVectorSpace(sentence0, sentence1, ic=False):
    pairs0, pairs1 = preprocessing(sentence0, sentence1)

    pairs0, pairs1 = set(pairs0), set(pairs1)

    allPairs = sorted(pairs0.union(pairs1))

    uS0 = _createVectorSpaceArray(pairs0, allPairs, ic)
    uS1 = _createVectorSpaceArray(pairs1, allPairs, ic)

    dot_p = np.dot(uS0, uS1)
    denominator = norm(uS0)*norm(uS1)
    cos_sim = abs(dot_p/denominator) if denominator != 0 else 0

    return cos_sim
