# Packages import
import re
import nltk
import spacy
import itertools
import numpy as np
from nltk.tokenize import TreebankWordTokenizer

nltk.download('punkt')
nltk.download('words')
nltk.download('wordnet')
nltk.download('omw-1.4')
nltk.download('stopwords')
nltk.download('treebank') # added for new preprocessing
nltk.download('averaged_perceptron_tagger')
nlp = spacy.load("en_core_web_sm")


# Remove slashes, hyphens, and underscores from the sentence
def stripPunctuation(sentence):
  characters_strip = "_-/\\><"
  return sentence.translate(str.maketrans('', '', characters_strip))


# Remove letters denoting a particular currency from monetary values
# For example, for $US1234.56 or US1234.56$, "US" will be 
# removed and we will be left with $1234.56 or 1234.56$
def simplifyCurrencies(sentence):
    sentenceSplitted = sentence.split(" ")

    # Check if any words for which a marker such as "US" comes before or after a monetary value
    pattern1 = re.compile("(\$)?[A-Za-z]{2}\d{1,}(\.)?(\d{2})?(\$)?")  
    pattern2 = re.compile("(\$)?\d{1,}(\.)?(\d{2})?[A-Za-z]{2}(\$)?")
        
    for i, word in enumerate(sentenceSplitted):
        if pattern1.match(word) or pattern2.match(word):
            # Delete the marker (e.g. "US")
            sentenceSplitted[i] = re.sub("[a-zA-Z]", "", word)
    
    return " ".join(sentenceSplitted)


# Tokenize the sentence using the indicated tokenizer
def tokenize(sentence, tokenizer):
  if tokenizer == 'PennTreebank':
    Tokenizer = TreebankWordTokenizer()
    tokens = Tokenizer.tokenize(sentence)

  if tokenizer == 'nltk':
    tokens = nltk.word_tokenize(sentence)

  if tokenizer == 'spacy':
    doc = nlp(sentence)
    with doc.retokenize() as retokenizer:
      tokens = [token for token in doc]

  return tokens


# Remove the apostrophe from contractions and replace it with the required letters to complete the word
# Tokenization automatically separates the contraction from the connected word, 
# so for example "didn’t" is tokenized into "did" and "n’t", with this last one the one to be replaced by "not".
def transformContractions(tokens):
    for token, i in zip(tokens, range(len(tokens))):

        if token == "n't":   # n't  -->  not
            tokens[i] = token.replace("'", "o")

            if tokens[i-1] == 'wo':
                tokens[i-1] = tokens[i-1].replace('o', 'ill')

        if token == "'m" or token == "'re":   # 'm or 're  -->  am or are
            tokens[i] = token.replace("'", "a")

        # This one could potentially cause problems when "'s" comes after a name.
        # For example "Daniel's book" would become "Daniel is book"
        if token == "'s":   # 's  -->  is
            tokens[i] = token.replace("'", "i")

        if token == "'ve":   # 've  -->  have
            tokens[i] = token.replace("'", "ha")

        # For this, the one concern is if the intention is "shall" as opposed to "will"
        if token == "'ll":   # 'll  -->  will
            tokens[i] = token.replace("'", "wi")

    return tokens


# Remove tokens that are entirely punctuation (not those that simply contain punctuations
# as we did in the earlier labs), with the exception of currency marker “$”
def removePunctuationTokens(tokens):
    # These are all the characters of string.punctuation except $
    test_punctuation = "!#%&'()*+,-./:;<=>?@[\]^_`{|}~"

    for token, i in zip(tokens, range(len(tokens))):
        # Checking whether token entirely punctuation (if so, remove it)
        if not token.isalnum() and not token.translate(str.maketrans('', '', test_punctuation)):   
            tokens.remove(token)

    return tokens


# Search the sentences to see if there are any words that are separate in one sentence, but combined in another. 
# If so, combine and add the words in the first sentence and delete the separated ones
def combineCompounds(tokens0, tokens1):
    for idx, (tokenPrev, token) in enumerate(zip(tokens0[:-1], tokens0[1:])):
        if tokenPrev+token in tokens1:
            tokens0.pop(idx); tokens0.pop(idx)
            tokens0.insert(idx,tokenPrev+token)

    for idx, (tokenPrev, token) in enumerate(zip(tokens1[:-1], tokens1[1:])):
        if tokenPrev+token in tokens0:
            tokens1.pop(idx); tokens1.pop(idx)
            tokens1.insert(idx,tokenPrev+token)

    return tokens0, tokens1


# Find part-of-speech tag to each of the tokens
def tagPOS(tokens):
    token_pairs = nltk.pos_tag(tokens)

    def _tagToSingle(tag):
        tag = tag.lower()
        pos = tag[0] # Noun ==> n,  Verbs ==> v
        if pos == 'j': pos = 'a' # Adjective = j ==> a
        if tag[:2] == 'rb': pos = 'r' # Adverb = rb ==> r
        return pos

    token_pairs = [(word,_tagToSingle(tag)) for (word,tag) in token_pairs]
    return token_pairs

# Remove the tokens matching an item in the stopwords list of the indicated corpus
def remove_stopwords(token_pairs, words_corpus, pairs=False):
  if words_corpus == 'english':
    stopWords = set(nltk.corpus.stopwords.words('english'))
    if pairs:
        return [(token,tag) for token,tag in token_pairs if (token.lower() not in stopWords)]
    else:
        return [token for token in token_pairs if (token.lower() not in stopWords)]

  if words_corpus == 'spacy':
    if pairs:
        return [(token,tag) for token,tag in token_pairs if not token.is_stop]
    else:
        return [token for token in token_pairs if not token.is_stop]


# Auxiliar function of the following main preprocessing function
def _preprocessingSentence(sentence, stripPunct=True, simplifyCurr=True, transformCont=True, 
                           tokenizer='PennTreebank', removePunctToken=True):
    sentence = sentence.lower()
    if stripPunct: sentence = stripPunctuation(sentence)
    if simplifyCurr: sentence = simplifyCurrencies(sentence)
    if tokenizer:
        tokens = tokenize(sentence, tokenizer=tokenizer)
        if transformCont: tokens = transformContractions(tokens)
        if removePunctToken: tokens = removePunctuationTokens(tokens)
        return tokens
    return sentence

   
# Main preprocessing function which converts one or two sentences two sentences, tokens or pairs
# with the desired preprocessing techniques applied or not. Some of them are requeriments
# to others, as the tokenization prior to the pos tagging.
def preprocessing(sentence1, sentence2=None, stripPunct=True, simplifyCurr=True, 
                  tokenizer='PennTreebank', transformCont=True, removePunctToken=True, 
                  combineComp=True, pos_tagger=True, remove_stop='english'):
    
    tokens1 = _preprocessingSentence(sentence1,stripPunct=stripPunct,simplifyCurr=simplifyCurr,
                                     tokenizer=tokenizer,transformCont=transformCont,
                                     removePunctToken=removePunctToken)
    if sentence2: tokens2 = _preprocessingSentence(sentence2,stripPunct=stripPunct,simplifyCurr=simplifyCurr,
                                                   tokenizer=tokenizer,transformCont=transformCont,
                                                   removePunctToken=removePunctToken)
    
    if tokenizer:
        if sentence2:
            if combineComp:
                tokens1, tokens2 = combineCompounds(tokens1, tokens2)
        
        if pos_tagger:
            token_pairs1 = tagPOS(tokens1)
            if sentence2: token_pairs2 = tagPOS(tokens2)
  
            if remove_stop:
                token_pairs1 = remove_stopwords(token_pairs1, words_corpus=remove_stop, pairs=True)
                if sentence2: token_pairs2 = remove_stopwords(token_pairs2, words_corpus=remove_stop, pairs=True)

            return (token_pairs1, token_pairs2) if sentence2 else token_pairs1
        
        if remove_stop:
            tokens1 = remove_stopwords(tokens1, words_corpus=remove_stop, pairs=False)
            if sentence2: tokens2 = remove_stopwords(tokens2, words_corpus=remove_stop, pairs=False)
        
    return (tokens1, tokens2) if sentence2 else tokens1
